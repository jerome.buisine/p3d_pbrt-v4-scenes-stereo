# Stereoscopic-scenes for pbrt-v4

## Stereo scenes and synthesis images

All synthesis images generated are of size `1920 x 1080` and saved into `.rawls` format for each sample estimated.

**Note:** `xxxx` means `left` or `right` eye view.

| Scene folder            | Filename                                 | Integrator | Sampler | Samples                  | Generated | Experiment step | Update reference  |
|-------------------------|------------------------------------------|------------|---------|--------------------------|-----------|-----------------|-------------------|
| `villa`                 | `p3d_villa-stereoscopic-xxxx.pbrt`       | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
